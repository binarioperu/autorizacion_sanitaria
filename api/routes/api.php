<?php

use FontLib\Table\Type\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/login','AuthController@login');
Route::get("parentesco/listar","ParentescoController@listar");
Route::get("tipo_documento/listar","TipoDocumentoController@listar");
Route::get("provincia/listar","ProvinciaController@listarPorDepartamento");
Route::get("funeraria/listar","FunerariaController@listar");
Route::get('distrito/listar/{provincia}', 'DistritoController@listarPorProvincia');
Route::get('establecimiento/listar/{provincia}/{distrito}', 'EstablecimientoController@listarPorProvinciaDistrito');
Route::get("ciex/listar","CiexController@listar");
Route::get("lugar_ocurrencia/listar","LugarOcurrenciaController@listar");
Route::get("tipo_autorizacion/listar","TipoAutorizacionController@listar");
Route::get("requisito/autorizacion/{id}/listar","RequisitosController@listarPorTipoAutorizacion");
Route::get("solicitante/dni/{dni}","SolicitanteController@solicitantePorDNI");
Route::get("fallecido/dni/{dni}","FallecidoController@fallecidoPorDNI");
Route::post('autorizacion/registrar', 'SolicitudController@registrar');
Route::post('downloadfile', 'SolicitudController@downloadAutorizacionxSolicitante');
Route::get("autorizacion_pendiente/listar","SolicitudController@listarpendientes");
Route::get("autorizacion_aprobada/listar","SolicitudController@listaraprobadas");
Route::get("autorizacion/actualizar/{estado}/{autorizacion_id}","SolicitudController@actualizarAutorizacion");
Route::get("autorizacionfinal/listar","SolicitudController@listarAutorizacionFinal");
Route::post('autorizacion/registrar_saneamiento_ambiental', 'SaneamientoAmbientalController@registrar');
