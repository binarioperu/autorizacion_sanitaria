<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funeraria extends Model
{
    protected $table = 'funerarias';
    public $timestamps = false;
}
