<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fallecido extends Model
{
    protected $table = 'fallecido';
    public $timestamps = false;
}
