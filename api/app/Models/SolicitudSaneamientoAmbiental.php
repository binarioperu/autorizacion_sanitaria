<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SolicitudSaneamientoAmbiental extends Model
{
    protected $table = 'solicitud_saneamiento_ambiental';
    public $timestamps = false;
}
