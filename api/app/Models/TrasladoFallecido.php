<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrasladoFallecido extends Model
{
    protected $table = 'traslado_fallecido';
    public $timestamps = false;
}
