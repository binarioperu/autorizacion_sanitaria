<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoAutorizacion extends Model
{
    protected $table = 'tipo_autorizacion';
    public $timestamps = false;
}
