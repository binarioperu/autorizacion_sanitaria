<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConservacionCremacion extends Model
{
    protected $table = 'conservacion_cremacion';
    public $timestamps = false;
}
