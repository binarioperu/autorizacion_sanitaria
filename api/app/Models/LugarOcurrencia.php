<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;

class LugarOcurrencia extends Model
{
    protected $table = 'lugar_ocurrencia';
    public $timestamps = false;
}
