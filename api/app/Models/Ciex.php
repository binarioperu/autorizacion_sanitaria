<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ciex extends Model
{
    protected $table = 'ciex';
    public $timestamps = false;
}
