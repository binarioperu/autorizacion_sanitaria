<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentoSaneamientoAmbiental extends Model
{
    
    protected $table = 'documentos_saneamiento_ambiental';
    public $timestamps = false;
}
