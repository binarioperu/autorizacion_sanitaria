<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConservacionInhumacion extends Model
{
    protected $table = 'conservacion_inhumacion';
    public $timestamps = false;
}
