<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Solicitante;

class SolicitanteController extends Controller
{
    public function solicitantePorDNI($dni)
    {
        try
        {
            $solicitante = Solicitante::where("dni",$dni)->where("activo",1)->first();
            $data = [
                "status" => true,
                "message" => "OK",
                "solicitante" => $solicitante,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "ERROR",
                "solicitante" => null,
                "code" => 0
            ];
            return response()->json($data,204);
        }
    }
}
