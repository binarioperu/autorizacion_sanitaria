<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;

class AuthController extends Controller
{
    public function login(Request $request){
        try{
            $usuario = Usuario::where('usuario',$request->usuario)->where('password',$request->password)->first();
            if ($usuario!=null){
                $data=[
                    'status'=>true,
                    'message'=>'OK',
                    'usuario'=>$usuario,
                    'code'=>1
                ];
                return response()->json($data,200);       
            }
            else{
                $data=[
                    'status'=>true,
                    'message'=>'OK',
                    'usuario'=>null,
                    'code'=>2
                ];
                return response()->json($data,200);           
            }
        }   
        catch(Exception $e){
            $data=[
                'status'=>false,
                'message'=>'error',
                'usuario'=>null,
                'code'=>0
            ];
            return response()->json($data,200);
        }     
    }
}
