<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parentesco;
use Exception;

class ParentescoController extends Controller
{
    public function listar()
    {
        try
        {
            $parentesco_lista = Parentesco::where("activo",1)->get();
            $data = [
                "status" => true,
                "message" => "OK",
                "parentesco" => $parentesco_lista,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "OK",
                "parentesco" => [],
                "code" => 0
            ];
            return response()->json($data,200);
        }
    }
}
