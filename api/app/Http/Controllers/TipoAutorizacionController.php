<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoAutorizacion;
use Exception;

class TipoAutorizacionController extends Controller
{
    public function listar()
    {
        try
        {
            $tipo_autorizacion_lista = TipoAutorizacion::where("activo",1)->orderBy('descripcion')->get();
            $data = [
                "status" => true,
                "message" => "OK",
                "tipo_autorizacion" => $tipo_autorizacion_lista,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "OK",
                "tipo_autorizacion" => [],
                "code" => 0
            ];
            return response()->json($data,200);
        }
    }
}
