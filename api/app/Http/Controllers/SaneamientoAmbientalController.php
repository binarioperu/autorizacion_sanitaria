<?php

namespace App\Http\Controllers;

use App\Models\DocumentoSaneamientoAmbiental;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Propietario;
use App\Models\SolicitudSaneamientoAmbiental;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;

class SaneamientoAmbientalController extends Controller
{
    public function registrar(Request $request)
    {
        try {
            DB::beginTransaction();
            $autorizacion = json_decode($request->autorizacion);

            $propietario = $this->crearObjetoPropietario($autorizacion);
            $empresa = $this->crearObjetoEmpresa($autorizacion, $propietario);
            $solicitud = $this->crearObjetoSolicitud($propietario, $empresa);
            $this->crearObjetoDocumentos($request, $solicitud, $autorizacion);
            DB::commit();
            $data = [
                "status" => true,
                "message" => "OK",
                "code" => 1,
                //"autorizacion" => $autorizacion_generada
            ];
            return response()->json($data, 200);
        } catch (Exception $e) {
            DB::rollback();
            $data = [
                "status" => false,
                "message" => "ERROR",
                "code" => 0,
            ];
            return response()->json($data, 200);
        }
    }

    public function crearObjetoPropietario($request)
    {
        $propietario = Propietario::where("dni", $request->dni_propietario)->first();
        if ($propietario) {
            return $propietario;
        } else {
            $propietario = new Propietario;
            $propietario->dni = $request->dni_propietario;
            $propietario->nombres = $request->nombres_propietario;
            $propietario->apellidos = $request->apellidos_propietario;
            $propietario->telefono = $request->telefono_propietario;
            $propietario->celular = $request->celular_propietario;
            $propietario->correo = $request->correo_propietario;
            $propietario->domicilio = $request->domicilio_propietario;
            $propietario->provincia = $request->provincia_propietario;
            $propietario->distrito = $request->distrito_propietario;
            $propietario->activo = true;
            $propietario->created_at = DB::raw("getdate()");
            $propietario->updated_at = DB::raw("getdate()");
            $propietario->save();
            return $propietario;
        }
    }

    public function crearObjetoEmpresa($request, $propietario)
    {
        $empresa = Empresa::where("numero_ruc", $request->numero_ruc)->first();
        if ($empresa) {
            return $empresa;
        } else {
            $arrayservicios = "";
            if ($request->servicios_saneamiento != null) {
                foreach ($request->servicios_saneamiento as $servicio) {
                    $arrayservicios = $arrayservicios . $servicio . ',';
                }
            }

            $empresa = new Empresa;
            $empresa->propietario_id = $propietario->id;
            $empresa->nombre_comercial = $request->nombre_comercial;
            $empresa->numero_ruc = $request->numero_ruc;
            $empresa->telefono = $request->telefono_empresa;
            $empresa->celular = $request->celular_empresa;
            $empresa->direccion = $request->direccion_empresa;
            $empresa->provincia = $request->provincia_empresa;
            $empresa->distrito = $request->distrito_empresa;
            $empresa->correo_electronico = $request->correo_electronico_empresa;
            $empresa->servicios_saneamiento = $arrayservicios;
            $empresa->activo = true;
            $empresa->created_at = DB::raw("getdate()");
            $empresa->updated_at = DB::raw("getdate()");
            $empresa->save();
            return $empresa;
        }
    }

    private function crearObjetoSolicitud($propietario, $empresa)
    {
        $solicitud = new SolicitudSaneamientoAmbiental;
        $solicitud->propietario_id = $propietario->id;
        $solicitud->empresa_id = $empresa->id;;
        $solicitud->estado = "pendiente";
        $solicitud->activo = true;
        $solicitud->created_at = DB::raw("getdate()");
        $solicitud->updated_at = DB::raw("getdate()");
        $solicitud->save();
        return $solicitud;
    }

    private function crearObjetoDocumentos($request, $solicitud, $autorizacion)
    {
        $archivos = $request->file('archivo');
        $requisitos = $request->requisito;
        Storage::disk('documentos_saneamiento_ambiental')->makeDirectory($autorizacion->nombre_comercial);

        for ($i = 0; $i < count($requisitos); $i++) {
            $nombrepdf = $this->generarNombreArchivo() . '.' . $archivos[$i]->getClientOriginalName();

            $documento = new DocumentoSaneamientoAmbiental;
            $documento->solicitud_id = $solicitud->id;
            $documento->requisito_id = $requisitos[$i];
            $documento->nombre_archivo = $nombrepdf;
            $documento->activo = true;
            $documento->created_at = DB::raw("getdate()");
            $documento->updated_at = DB::raw("getdate()");
            $documento->save();

            Storage::disk("documentos_saneamiento_ambiental")->put($autorizacion->nombre_comercial . '/' . $nombrepdf, File::get($archivos[$i]));
        }
    }

    private function generarNombreArchivo()
    {
        $fecha = new Carbon();
        $fechaHoy = $fecha->format('YmdHis');
        $nombreFoto = $fechaHoy;
        return $nombreFoto;
    }
}
