<?php

namespace App\Http\Controllers;

use App\Models\Funeraria;
use Illuminate\Http\Request;

class FunerariaController extends Controller
{
    public function listar()
    {
        try
        {
            $funeraria_list = Funeraria::where("activo",1)->get();
            $data = [
                "status" => true,
                "message" => "OK",
                "funeraria" => $funeraria_list,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "OK",
                "funeraria" => [],
                "code" => 0
            ];
            return response()->json($data,200);
        }
    }
}
