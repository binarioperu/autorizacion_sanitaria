<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ciex;
use Exception;

class CiexController extends Controller
{
    public function listar()
    {
        try
        {
            $ciex_listar = Ciex::all();
            $data = [
                "status" => true,
                "message" => "OK",
                "ciex" => $ciex_listar,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "ERROR",
                "ciex" => [],
                "code" => 0
            ];
            return response()->json($data,200);
        }
    }
}
