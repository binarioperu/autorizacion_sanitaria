<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LugarOcurrencia;
use Exception;

class LugarOcurrenciaController extends Controller
{
    public function listar()
    {
        try
        {
            $lugar_ocurrencia_lista = LugarOcurrencia::where("activo",1)->get();
            $data = [
                "status" => true,
                "message" => "OK",
                "lugar_ocurrencia" => $lugar_ocurrencia_lista,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "OK",
                "lugar_ocurrencia" => [],
                "code" => 0
            ];
            return response()->json($data,200);
        }
    }
}
