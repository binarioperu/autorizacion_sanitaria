<?php

namespace App\Http\Controllers;

use App\Models\TipoDocumento;
use Illuminate\Http\Request;

class TipoDocumentoController extends Controller
{
    public function listar()
    {
        try
        {
            $tipo_documento_lista = TipoDocumento::where("activo",1)->get();
            $data = [
                "status" => true,
                "message" => "OK",
                "tipo_documento" => $tipo_documento_lista,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "OK",
                "tipo_documento" => [],
                "code" => 0
            ];
            return response()->json($data,200);
        }
    }
}
