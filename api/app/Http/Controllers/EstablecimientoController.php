<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class EstablecimientoController extends Controller
{

    public function listarPorProvinciaDistrito($provincia, $distrito)
    {
        try {
            $establecimientos_lista = DB::select("select codigo_unico,nombre_establecimiento from establecimientos where provincia = :provincia AND distrito = :distrito group by codigo_unico, nombre_establecimiento", ['provincia' => $provincia, 'distrito' => $distrito]);
            $data = [
                "status" => true,
                "message" => "OK",
                "establecimiento" => $establecimientos_lista,
                "code" => 1
            ];
            return response()->json($data);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "ERROR",
                "establecimiento" => [],
                "code" => 1
            ];
            return response()->json($data, 204);
        }
    }
}
