<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class ProvinciaController extends Controller
{
    public function listarPorDepartamento()
    {
        try
        {
            $provincia_lista = DB::select("select provincia from establecimientos group by provincia");
            $data = [
                "status" => true,
                "message" => "OK",
                "provincia" => $provincia_lista,
                "code" => 1
            ];
            return response()->json($data);
        }
        catch (Exception $e)
        {
            $data = [
                "status" => false,
                "message" => "ERROR",
                "provincia" => [],
                "code" => 0
            ];
            return response()->json($data,204);
        }
    }
}
