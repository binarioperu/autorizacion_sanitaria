<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Requisito;
use Exception;

class RequisitosController extends Controller
{
    public function listarPorTipoAutorizacion($id)
    {
        try
        {
            $requisito_lista = Requisito::where("tipo_autorizacion_id",$id)->where("activo",1)->get();
            $data = [
                "status" => true,
                "message" => "OK",
                "requisito" => $requisito_lista,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "ERROR",
                "requisito" => [],
                "code" => 0
            ];
            return response()->json($data,200);
        }
    }
}
