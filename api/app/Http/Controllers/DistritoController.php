<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class DistritoController extends Controller
{
    public function listarPorProvincia($provincia)
    {
        try
        {
            $distrito_lista = DB::select("select distrito from establecimientos where provincia = :provincia group by distrito", ['provincia' => $provincia]);
            $data = [
                "status" => true,
                "message" => "OK",
                "distrito" => $distrito_lista,
                "code" => 1
            ];
            return response()->json($data);
        }
        catch (Exception $e)
        {
            $data = [
                "status" => false,
                "message" => "ERROR",
                "distrito" => [],
                "code" => 1
            ];
            return response()->json($data,204);
        }
    }
}
