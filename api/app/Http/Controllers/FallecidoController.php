<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fallecido;
use Exception;

class FallecidoController extends Controller
{
    public function fallecidoPorDNI($dni)
    {
        try
        {
            $fallecido = Fallecido::where("dni",$dni)->where("activo",1)->first();
            $data = [
                "status" => true,
                "message" => "OK",
                "fallecido" => $fallecido,
                "code" => 1
            ];
            return response()->json($data,200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "ERROR",
                "fallecido" => null,
                "code" => 0
            ];
            return response()->json($data,204);
        }
    }
}
