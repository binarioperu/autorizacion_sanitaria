<?php

namespace App\Http\Controllers;

use App\Mail\SendEmailAutorizacion;
use App\Models\ConservacionCremacion;
use App\Models\ConservacionInhumacion;
use App\Models\Documento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Solicitante;
use App\Models\Fallecido;
use App\Models\Medico;
use App\Models\Solicitud;
use App\Models\TrasladoFallecido;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;
use ZipArchive;

class SolicitudController extends Controller
{
    public function registrar(Request $request)
    {
        try {
            DB::beginTransaction();
            $autorizacion = json_decode($request->autorizacion);

            $solicitante = $this->crearObjetoSolicitante($autorizacion);
            $medico = $this->crearObjetoMedico($autorizacion);
            $fallecido = $this->crearObjetoFallecido($autorizacion, $medico);
            $solicitud = $this->crearObjetoSolicitud($autorizacion, $solicitante, $fallecido);
            $this->crearDetalleSolicitud($autorizacion, $solicitud);
            $this->crearObjetoDocumentos($request, $solicitud, $autorizacion);
            DB::commit();
            $data = [
                "status" => true,
                "message" => "OK",
                "code" => 1,
                //"autorizacion" => $autorizacion_generada
            ];
            return response()->json($data, 200);
        } catch (Exception $e) {
            DB::rollback();
            $data = [
                "status" => false,
                "message" => "ERROR",
                "code" => 0,
            ];
            return response()->json($data, 200);
        }
    }

    private function crearObjetoSolicitante($request)
    {
        $solicitante = Solicitante::where("dni", $request->dni_solicitante)->first();
        if ($solicitante) {
            return $solicitante;
        } else {
            $solicitante = new Solicitante;
            $solicitante->dni = $request->dni_solicitante;
            $solicitante->nombres = $request->nombres_solicitante;
            $solicitante->apellidos = $request->apellidos_solicitante;
            $solicitante->parentesco_id = $request->parentesco;
            $solicitante->tipo_documento_id = $request->tipo_documento;
            $solicitante->direccion = $request->direccion_solicitante;
            $solicitante->telefono = $request->telefono_solicitante;
            $solicitante->correo = $request->correo_solicitante;
            $solicitante->activo = true;
            $solicitante->created_at = DB::raw("getdate()");
            $solicitante->updated_at = DB::raw("getdate()");
            $solicitante->save();
            return $solicitante;
        }
    }

    private function crearObjetoMedico($request)
    {
        $medico = new Medico;
        $medico->dni = $request->dni_medico;
        $medico->nombres = $request->nombres_medico;
        $medico->apellidos = $request->apellidos_medico;
        $medico->activo = true;
        $medico->created_at = DB::raw("getdate()");
        $medico->updated_at = DB::raw("getdate()");
        $medico->save();
        return $medico;
    }

    private function crearObjetoFallecido($request, $medico)
    {
        $fallecido = new Fallecido;
        $fallecido->medico_id = $medico->id;
        $fallecido->lugar_ocurrencia_id = $request->lugar_ocurrencia;
        $fallecido->id_ciex = $request->ciex;
        $fallecido->dni = $request->dni_fallecido;
        $fallecido->nombres = $request->nombres_fallecido;
        $fallecido->apellidos = $request->apellidos_fallecido;
        $fallecido->edad = $request->edad_fallecido;
        if ($request->establecimiento == null) {
            $fallecido->direccion = $request->direccion_fallecimiento;
        } else {
            $fallecido->direccion = $request->establecimiento;
        }
        $fallecido->fecha_fallecimiento = $request->fecha_fallecimiento;
        $fallecido->provincia = $request->provincia;
        $fallecido->distrito =  $request->distrito;
        $fallecido->activo = true;
        $fallecido->created_at = DB::raw("getdate()");
        $fallecido->updated_at = DB::raw("getdate()");
        $fallecido->save();
        return $fallecido;
    }

    private function crearObjetoSolicitud($request, $solicitante, $fallecido)
    {
        $solicitud = new Solicitud;
        $solicitud->solicitante_id = $solicitante->id;
        $solicitud->fallecido_id = $fallecido->id;
        $solicitud->tipo_autorizacion_id = $request->tipo_autorizacion;
        $solicitud->fecha = $request->fecha_inhumacion_cremacion;
        $solicitud->hora = $request->hora_inhumacion_cremacion;
        $solicitud->direccion = $request->lugar_inhumacion_cremacion;
        $solicitud->estado = "en revisión";
        $solicitud->activo = true;
        $solicitud->created_at = DB::raw("getdate()");
        $solicitud->updated_at = DB::raw("getdate()");
        $solicitud->save();
        return $solicitud;
    }

    private function generarNombreArchivo()
    {
        $fecha = new Carbon();
        $fechaHoy = $fecha->format('YmdHis');
        $nombreFoto = $fechaHoy;
        return $nombreFoto;
    }

    private function crearDetalleSolicitud($request, $solicitud)
    {
        if ($request->tipo_autorizacion == 4 || $request->tipo_autorizacion == 5) {
            $traslado_fallecido = new TrasladoFallecido;
            $traslado_fallecido->solicitud_id = $solicitud->id;
            $traslado_fallecido->localidad_origen = $request->localidad_origen;
            $traslado_fallecido->distrito_origen = $request->distrito_origen;
            $traslado_fallecido->provincia_origen = $request->provincia_origen;
            $traslado_fallecido->localidad_destino = $request->localidad_destino;
            $traslado_fallecido->distrito_destino = $request->distrito_destino;
            $traslado_fallecido->provincia_destino = $request->provincia_destino;
            $traslado_fallecido->tipo_entierro = $request->tipo_entierro;
            $traslado_fallecido->activo = true;
            $traslado_fallecido->created_at = DB::raw("getdate()");
            $traslado_fallecido->updated_at = DB::raw("getdate()");
            $traslado_fallecido->save();
        } else if ($request->tipo_autorizacion == 6) {
            $conservacion_cremacion = new ConservacionCremacion;
            $conservacion_cremacion->solicitud_id = $solicitud->id;
            $conservacion_cremacion->verificacion_cadaver = $request->verificacion_cadaver;
            $conservacion_cremacion->instalaciones = $request->instalaciones;
            $conservacion_cremacion->personal = $request->personal;
            $conservacion_cremacion->compuesto = $request->compuesto_aplicado_cre;
            $conservacion_cremacion->servicio = $request->servicio;
            $conservacion_cremacion->patologo = $request->patologo;
            $conservacion_cremacion->certificado_necropsia = $request->certificado_necropsia;
            $conservacion_cremacion->activo = true;
            $conservacion_cremacion->created_at = DB::raw("getdate()");
            $conservacion_cremacion->updated_at = DB::raw("getdate()");
            $conservacion_cremacion->save();
        } else if ($request->tipo_autorizacion == 3) {
            $conservacion_inhumacion = new ConservacionInhumacion;
            $conservacion_inhumacion->solicitud_id = $solicitud->id;
            $conservacion_inhumacion->funeraria = $request->funeraria;
            $conservacion_inhumacion->ubicacion_funeraria = $request->ubicacion_funeraria;
            $conservacion_inhumacion->distrito_funeraria = $request->distrito_funeraria;
            $conservacion_inhumacion->provincia_funeraria = $request->provincia_funeraria;
            $conservacion_inhumacion->compuesto = $request->compuesto_aplicado_inh;
            $conservacion_inhumacion->activo = true;
            $conservacion_inhumacion->created_at = DB::raw("getdate()");
            $conservacion_inhumacion->updated_at = DB::raw("getdate()");
            $conservacion_inhumacion->save();
        } else {
            return;
        }
    }

    private function crearObjetoDocumentos($request, $solicitud, $autorizacion)
    {
        $archivos = $request->file('archivo');
        $requisitos = $request->requisito;
        Storage::disk('documentos_fallecido')->makeDirectory($autorizacion->nombres_fallecido . ' ' . $autorizacion->apellidos_fallecido);

        for ($i = 0; $i < count($requisitos); $i++) {
            $nombrepdf = $this->generarNombreArchivo() . '.' . $archivos[$i]->getClientOriginalName();

            $documento = new Documento;
            $documento->solicitud_id = $solicitud->id;
            $documento->requisitos_id = $requisitos[$i];
            $documento->nombre_archivo = $nombrepdf;
            $documento->activo = true;
            $documento->created_at = DB::raw("getdate()");
            $documento->updated_at = DB::raw("getdate()");
            $documento->save();

            Storage::disk("documentos_fallecido")->put($autorizacion->nombres_fallecido . ' ' . $autorizacion->apellidos_fallecido . '/' . $nombrepdf, File::get($archivos[$i]));
        }
    }

    public function guardarPDF($data)
    {
        $pdf = PDF::loadView('autorizacion', ['data' => $data]);
        $nombrearchivo = 'AUTORIZACION_SANITARIA_' . $this->generarNombreArchivo() . '.pdf';
        Storage::disk("documentos_fallecido")->put($nombrearchivo, $pdf->output());
        return $nombrearchivo;
    }

    public function downloadAutorizacionxSolicitante(Request $request)
    {
        try {
            $zip = new ZipArchive;
            $filenamezip = $request->nombre_fallecido . '.zip';
            if ($zip->open(public_path('documentos_fallecido/' . $filenamezip), ZipArchive::CREATE) === TRUE) {
                $files = File::files(public_path('documentos_fallecido/' . $request->nombre_fallecido));

                foreach ($files as $key => $value) {
                    $relativeNameInZipFile = basename($value);
                    $zip->addFile($value, $relativeNameInZipFile);
                }

                $zip->close();
            }

            return response()->download(public_path('documentos_fallecido/' . $filenamezip), $filenamezip);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['type' => 0], 204);
        }
    }

    public function listarpendientes()
    {
        try {
            $autorizaciones_list = DB::select("exec sp_lista_autorizacion_sanitaria");
            $data = [
                "status" => true,
                "message" => "OK",
                "autorizaciones" => $autorizaciones_list,
                "code" => 1
            ];
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "OK",
                "autorizaciones" => [],
                "code" => 0
            ];
            return response()->json($data, 200);
        }
    }

    public function listaraprobadas()
    {
        try {
            $autorizaciones_list = DB::select("exec sp_lista_autorizacion_sanitaria_aprobado");
            $data = [
                "status" => true,
                "message" => "OK",
                "autorizaciones" => $autorizaciones_list,
                "code" => 1
            ];
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "OK",
                "autorizaciones" => [],
                "code" => 0
            ];
            return response()->json($data, 200);
        }
    }
    

    public function actualizarAutorizacion($estado, $autorizacion_id)
    {
        try {
            $autorizacion = Solicitud::where("id", $autorizacion_id)->first();
            DB::beginTransaction();
            $this->updateObjetoAutorizacion($autorizacion, $estado);
            if ($estado == 'aprobado') {
                $dataregistrada = DB::select("exec sp_datasolicitud ?", array($autorizacion_id));
                $autorizacion_generada = $this->guardarPDF($dataregistrada[0]);
                Mail::to($dataregistrada[0]->correo)->send(new SendEmailAutorizacion($autorizacion_generada));
            }
            DB::commit();
            $data = [
                "status" => true,
                "message" => "OK",
                "code" => 1
            ];
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "ERROR",
                "code" => 0
            ];
            return response()->json($data, 200);
        }
    }
    private function updateObjetoAutorizacion($autorizacion, $estado)
    {
        $autorizacion->estado = $estado;
        $autorizacion->updated_at = DB::raw("getdate()");
        $autorizacion->save();
    }
    public function listarAutorizacionFinal()
    {
        try {
            $autorizaciones_list = DB::select("exec sp_lista_autorizacion_sanitaria_final");
            $data = [
                "status" => true,
                "message" => "OK",
                "autorizaciones" => $autorizaciones_list,
                "code" => 1
            ];
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "OK",
                "autorizaciones" => [],
                "code" => 0
            ];
            return response()->json($data, 200);
        }
    }
}
