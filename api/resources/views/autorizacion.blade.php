<div class="col-md-12" style="    font-family: Helvetica;">
    <div style="float: left;
    width: 100%;
    margin-bottom: 1rem;">
        <img src="{{ public_path('images/logodiresa.png') }}">
        <img src="{{ public_path('images/juntosporlaprosperidad.png') }}" style="    float: right;">
    </div>
    <p style="font-weight: bold;font-style: italic;font-size: 16px;text-align: center;">
        "Año de la Universalización de la Salud"</p>
    <p style="text-align: center;font-size: 17px;line-height: 17px;">LA SUB GERENCIA DE PROMOCIÓN DE
        GESTIÓN
        TERRITORIAL DE LA
        GERENCIA
        REGIONAL DE SALUD LA LIBERTAD, OTORGA:
    </p>
    <p style="text-decoration: underline;text-align: center;font-size: 16.5px;">
        <b>AUTORIZACIÓN SANITARIA DE
            INHUMACIÓN DE CADÁVER ANTES DE LAS 24 HORAS
            Nº {{$data->correlativo}}-2020 -GRS-SGPGT</b>
    </p>
    <p style="line-height: 25px; font-size: 16px;text-align: justify;">Que,
        {{$data->nombre_solicitante}},
        identificado con DNI, N° {{$data->dni_solicitante}},
        en calidad de {{$data->parentesco}}, con domicilio en
        {{$data->direccion_solicitante}},
        distrito {{$data->distrito}}, provincia {{$data->provincia}}, departamento La
        Libertad;
        ha cumplido con los requisitos exigidos por Ley para realizar
        <b>{{$data->tipo_autorizacion}}</b>, quien
        en
        vida fue {{$data->nombre_fallecido}}, identificado con DNI, N°
        {{$data->dni_fallecido}}, fallecido(a) el
        {{Date::createFromDate($data->fecha_fallecimiento)->format('l j F Y')}}, edad
        {{$data->edad}} años, la causa básica de muerte: {{$data->causa_muerte}}, tal y
        como
        consta en
        el
        certificado
        de defunción general, suscrito por el médico {{$data->nombre_medico}} ocurrido en
        {{$data->lugar_ocurrencia}} ubicado en {{$data->direccion_fallecido}} distrito
        {{$data->distrito}}, provincia {{$data->provincia}}, departamento La Libertad.
    </p>
    <p>
        Dicho Cadáver será {{$data->procedimiento}} en el <b>CEMENTERIO</b> XXXXXX
        ubicado en {{$data->ubicacion_cementerio}}, distrito {{$data->distrito}},
        provincia
        {{$data->provincia}}, departamento La Libertad;
        el día {{Date::createFromDate($data->fecha_entierro)->format('l j F Y')}} a las
        {{$data->hora_entierro}}
        horas.
    </p>
    <p>
        La presente Autorización Sanitaria para la <b>INHUMACIÓN DE CADÁVER ANTES DEL PLAZO DE LEY</b>,
        es
        en
        aplicación del
        artículo 115, de la Ley Nº26842 - Ley General de Salud; y el Título III, Capitulo III, art. 49º
        sobre
        inhumaciones del D.S. Nº03-
        94-SA del Reglamento de la Ley de Cementerios y Servicios Funerarios, Ley Nº26298
    </p>
    <p>
        Se expide la presente a solicitud de la parte interesada, para los fines que estime necesarios,
        no
        siendo
        válido para acciones judiciales en contra del Estado Peruano.
    </p>
    <p>
        Trujillo, {{Date::createFromDate($data->fecha_registro)->format('l j F Y')}}
    </p>
    <hr>
    <p style="font-weight: bold;font-style: italic;font-size: 20px;text-align: center;">
        "Juntos por la Prosperidad"</p>
    <p style="text-align: center;font-size: 12px;">Local Institucional: Los Rubíes N°517- 519 y Las
        Esmeraldas Nº 403 – Urb. Santa Inés</p>
</div>