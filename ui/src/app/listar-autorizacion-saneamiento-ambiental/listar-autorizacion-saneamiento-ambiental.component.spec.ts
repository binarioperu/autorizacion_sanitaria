import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarAutorizacionSaneamientoAmbientalComponent } from './listar-autorizacion-saneamiento-ambiental.component';

describe('ListarAutorizacionSaneamientoAmbientalComponent', () => {
  let component: ListarAutorizacionSaneamientoAmbientalComponent;
  let fixture: ComponentFixture<ListarAutorizacionSaneamientoAmbientalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarAutorizacionSaneamientoAmbientalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarAutorizacionSaneamientoAmbientalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
