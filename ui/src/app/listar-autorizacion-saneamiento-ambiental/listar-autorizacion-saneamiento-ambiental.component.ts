import { Component, OnInit } from '@angular/core';
import { AutorizacionService } from '../services/autorizacion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-autorizacion-saneamiento-ambiental',
  templateUrl: './listar-autorizacion-saneamiento-ambiental.component.html',
  styleUrls: ['./listar-autorizacion-saneamiento-ambiental.component.css']
})
export class ListarAutorizacionSaneamientoAmbientalComponent implements OnInit {

  constructor(private autorizacionService: AutorizacionService) { }

  preloadingstate;
  autorizaciones_list;

  ngOnInit(): void {
    this.preloadingstate = true;
    this.autorizacionService.listarAutorizacionesPendientes()
      .subscribe((data) => {
        this.autorizaciones_list = data.autorizaciones;
        this.preloadingstate = false;
      });
  }
  updateEstado(estado, autorizacion_id) {
    Swal.fire({
      title: '¿Estás seguro?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, adelante!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.preloadingstate = true;
        try {
          this.autorizacionService.updateAutorizacion(estado, autorizacion_id)
            .subscribe((data) => {
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Solicitud' + estado + 'con éxito',
              })
              this.preloadingstate = false;
            },
              err => console.log(err),
            );
        }
        catch (e) {
          console.log(e);
        }
      }
    })
  }
  descargarAutorizacion(data) {
    this.preloadingstate = true;
    this.autorizacionService.getFile(data).subscribe(
      response => {
        var downloadURL = window.URL.createObjectURL(response);
        var link = document.createElement('a');
        link.href = downloadURL;
        link.download = data.nombre_fallecido;
        link.click()
      }
    )
  }
}
