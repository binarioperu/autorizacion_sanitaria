import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AutorizacionService } from '../services/autorizacion.service';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-autorizacion-sanitaria',
  templateUrl: './autorizacion-sanitaria.component.html',
  styleUrls: ['./autorizacion-sanitaria.component.css']
})
export class AutorizacionSanitariaComponent implements OnInit {
  title = 'SOLICITUD PARA LA AUTORIZACIÓN SANITARIA';
  registrarSubmitted;
  buscarSolicitanteSubmitted;
  buscarFallecidoSubmitted;
  buscarMedicoSubmitted;
  registroForm: FormGroup;
  detalleautorizacionForm: FormGroup;
  preloadingstate: boolean;
  provincia_list;
  distrito_list;
  distritos_list_origen;
  distritos_list_destino;
  distritos_list_funeraria;
  parentesco_list;
  tipo_documento_list;
  ciex_list;
  lugar_ocurrencia_list;
  tipo_autorizacion_list;
  funeraria_list;
  archivoRequisito;
  requisito_list;
  solicitante = null;
  fallecido = null;
  medico = null;
  mostrardiv;
  establecimiento_list;
  showestablecimiento;
  tipo_entierro = [{ id: 1, tipo: 'INHUMACIÓN' }, { id: 2, tipo: 'CREMACIÓN' }]
  tipo_conservacion = [{ id: 1, tipo: 'FORMOLIZACIÓN' }, { id: 2, tipo: 'MINIFRIGO' }]
  tipo_necropsia = [{ id: 1, tipo: 'Necropsia clínica' }, { id: 2, tipo: 'Autorización cremación' }]
  constructor(private formBuilder: FormBuilder, private titleService: Title, private autorizacionService: AutorizacionService) { }

  ngOnInit(): void {
    this.preloadingstate = true;
    this.showestablecimiento = false;
    this.registrarSubmitted = false;
    this.buscarSolicitanteSubmitted = false;
    this.buscarFallecidoSubmitted = false;
    this.buscarMedicoSubmitted = false;
    this.archivoRequisito = [];
    this.lugar_ocurrencia_list = [];
    this.tipo_autorizacion_list = [];
    this.funeraria_list = [];
    this.requisito_list = [];
    this.ciex_list = [];
    this.parentesco_list = [];
    this.tipo_documento_list = [];
    this.distrito_list = [];
    this.provincia_list = [];
    this.mostrardiv = 'datossolicitante';
    this.showtipo_conservacion = false;
    this.showtipo_necropsia = false;
    this.showtipo_necropsia_mp = false;
    this.titleService.setTitle(this.title);
    this.beginFormBuilder();

    this.autorizacionService.listarCiex()
      .subscribe((data) => {
        this.ciex_list = data.ciex;
      });
    this.autorizacionService.listarProvincia()
      .subscribe((data) => {
        this.provincia_list = data.provincia;
      });
    this.autorizacionService.listarParentesco()
      .subscribe((data) => {
        this.parentesco_list = data.parentesco;
      });
    this.autorizacionService.listarTipoDocumento()
      .subscribe((data) => {
        this.tipo_documento_list = data.tipo_documento;
      });
    this.autorizacionService.listarLugarOcurrencia()
      .subscribe((data) => {
        this.lugar_ocurrencia_list = data.lugar_ocurrencia;
      });
    this.autorizacionService.listarTipoAutorizacion()
      .subscribe((data) => {
        this.tipo_autorizacion_list = data.tipo_autorizacion;
      });
      this.autorizacionService.listarFuneraria()
      .subscribe((data) => {
        this.funeraria_list = data.funeraria;
      });
    this.setUserCategoryValidators();
    this.showEstablecimiento();
    this.preloadingstate = false;
  }

  beginFormBuilder() {
    this.registroForm = this.formBuilder.group({
      dni_solicitante: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      nombres_solicitante: [{ value: '', disabled: true }, Validators.required],
      apellidos_solicitante: [{ value: '', disabled: true }, Validators.required],
      telefono_solicitante: [{ value: '', disabled: true }, Validators.required],
      correo_solicitante: [{ value: '', disabled: true }, Validators.required],
      direccion_solicitante: [{ value: '', disabled: true }, Validators.required],
      parentesco: [{ value: '', disabled: true }, Validators.required],
      tipo_documento: [{ value: '', }, Validators.required],

      dni_fallecido: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      nombres_fallecido: [{ value: '', disabled: true }, Validators.required],
      apellidos_fallecido: [{ value: '', disabled: true }, Validators.required],
      edad_fallecido: [{ value: '', disabled: true }, Validators.required],
      fecha_fallecimiento: [{ value: '', disabled: true }, Validators.required],
      lugar_ocurrencia: [{ value: '', disabled: true }, Validators.required],
      provincia: [{ value: '', disabled: true }, Validators.required],
      distrito: [{ value: '', disabled: true }, Validators.required],
      establecimiento: [{ value: '', disabled: true }, Validators.required],
      direccion_fallecimiento: [{ value: '', disabled: true }, Validators.required],
      ciex: [{ value: null, disabled: true }, Validators.required],

      dni_medico: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      nombres_medico: [{ value: '', disabled: true }, Validators.required],
      apellidos_medico: [{ value: '', disabled: true }, Validators.required],

      tipo_autorizacion: ['', Validators.required],
      fecha_inhumacion_cremacion: ['', Validators.required],
      hora_inhumacion_cremacion: ['', Validators.required],
      lugar_inhumacion_cremacion: ['', Validators.required],

      localidad_origen: ['', Validators.required],
      provincia_origen: ['', Validators.required],
      distrito_origen: ['', Validators.required],
      localidad_destino: ['', Validators.required],
      provincia_destino: ['', Validators.required],
      distrito_destino: ['', Validators.required],
      tipo_entierro: ['', Validators.required],

      compuesto_aplicado_cre: ['', Validators.required],

      funeraria: ['', Validators.required],
      ubicacion_funeraria: ['', Validators.required],
      distrito_funeraria: ['', Validators.required],
      provincia_funeraria: ['', Validators.required],
      tipo_conservacion_inh: ['', Validators.required],
      compuesto_aplicado_inh: ['', Validators.required],

      tipo_conservacion: ['', Validators.required],
      tipo_necropsia: ['', Validators.required],
      entidad: ['', Validators.required],
      nombre_patologo: ['', Validators.required],
      certificado_necropsia: ['', Validators.required],
    });

    this.f.dni_solicitante.value
  }

  get f() { return this.registroForm.controls; }

  setUserCategoryValidators() {
    this.registroForm.get('tipo_autorizacion').valueChanges
      .subscribe(tipo_autorizacion => {
        this.registroForm.get('localidad_origen').reset();
        this.registroForm.get('provincia_origen').reset();
        this.registroForm.get('distrito_origen').reset();
        this.registroForm.get('localidad_destino').reset();
        this.registroForm.get('provincia_destino').reset();
        this.registroForm.get('distrito_destino').reset();
        this.registroForm.get('tipo_entierro').reset();

        this.registroForm.get('compuesto_aplicado_cre').reset();

        this.registroForm.get('funeraria').reset();
        this.registroForm.get('ubicacion_funeraria').reset();
        this.registroForm.get('distrito_funeraria').reset();
        this.registroForm.get('provincia_funeraria').reset();
        this.registroForm.get('tipo_conservacion_inh').reset();
        this.registroForm.get('compuesto_aplicado_inh').reset();

        this.registroForm.get('tipo_conservacion').reset();
        this.registroForm.get('tipo_necropsia').reset();
        this.registroForm.get('certificado_necropsia').reset();
        this.registroForm.get('entidad').reset();
        this.registroForm.get('nombre_patologo').reset();

        if (tipo_autorizacion === '4' || tipo_autorizacion === '5') {
          this.registroForm.get('localidad_origen').setValidators([Validators.required]);
          this.registroForm.get('provincia_origen').setValidators([Validators.required]);
          this.registroForm.get('distrito_origen').setValidators([Validators.required]);
          this.registroForm.get('localidad_destino').setValidators([Validators.required]);
          this.registroForm.get('provincia_destino').setValidators([Validators.required]);
          this.registroForm.get('distrito_destino').setValidators([Validators.required]);
          this.registroForm.get('tipo_entierro').setValidators([Validators.required]);

          this.registroForm.get('compuesto_aplicado_cre').setValidators(null);

          this.registroForm.get('funeraria').setValidators(null);
          this.registroForm.get('ubicacion_funeraria').setValidators(null);
          this.registroForm.get('distrito_funeraria').setValidators(null);
          this.registroForm.get('provincia_funeraria').setValidators(null);
          this.registroForm.get('tipo_conservacion_inh').setValidators(null);
          this.registroForm.get('compuesto_aplicado_inh').setValidators(null);

          this.registroForm.get('tipo_conservacion').setValidators(null);
          this.registroForm.get('tipo_necropsia').setValidators(null);
          this.registroForm.get('certificado_necropsia').setValidators(null);
          this.registroForm.get('entidad').setValidators(null);
          this.registroForm.get('nombre_patologo').setValidators(null);

        } else if (tipo_autorizacion === '3') {
          this.registroForm.get('localidad_origen').setValidators(null);
          this.registroForm.get('provincia_origen').setValidators(null);
          this.registroForm.get('distrito_origen').setValidators(null);
          this.registroForm.get('localidad_destino').setValidators(null);
          this.registroForm.get('provincia_destino').setValidators(null);
          this.registroForm.get('distrito_destino').setValidators(null);
          this.registroForm.get('tipo_entierro').setValidators(null);

          this.registroForm.get('compuesto_aplicado_cre').setValidators(null);

          this.registroForm.get('tipo_conservacion').setValidators(null);
          this.registroForm.get('tipo_necropsia').setValidators(null);
          this.registroForm.get('certificado_necropsia').setValidators(null);
          this.registroForm.get('entidad').setValidators(null);
          this.registroForm.get('nombre_patologo').setValidators(null);

          this.registroForm.get('funeraria').setValidators([Validators.required]);
          this.registroForm.get('ubicacion_funeraria').setValidators([Validators.required]);
          this.registroForm.get('distrito_funeraria').setValidators([Validators.required]);
          this.registroForm.get('provincia_funeraria').setValidators([Validators.required]);
          this.registroForm.get('tipo_conservacion_inh').setValidators([Validators.required]);
          this.registroForm.get('compuesto_aplicado_inh').setValidators([Validators.required]);
        } else if (tipo_autorizacion === '6') {
          this.registroForm.get('localidad_origen').setValidators(null);
          this.registroForm.get('provincia_origen').setValidators(null);
          this.registroForm.get('distrito_origen').setValidators(null);
          this.registroForm.get('localidad_destino').setValidators(null);
          this.registroForm.get('provincia_destino').setValidators(null);
          this.registroForm.get('distrito_destino').setValidators(null);
          this.registroForm.get('tipo_entierro').setValidators(null);

          this.registroForm.get('compuesto_aplicado_cre').setValidators([Validators.required]);

          this.registroForm.get('funeraria').setValidators(null);
          this.registroForm.get('ubicacion_funeraria').setValidators(null);
          this.registroForm.get('distrito_funeraria').setValidators(null);
          this.registroForm.get('provincia_funeraria').setValidators(null);
          this.registroForm.get('tipo_conservacion_inh').setValidators(null);
          this.registroForm.get('compuesto_aplicado_inh').setValidators(null);

          this.registroForm.get('tipo_conservacion').setValidators([Validators.required]);
          this.registroForm.get('tipo_necropsia').setValidators([Validators.required]);
          this.registroForm.get('certificado_necropsia').setValidators([Validators.required]);
          this.registroForm.get('entidad').setValidators([Validators.required]);
          this.registroForm.get('nombre_patologo').setValidators([Validators.required]);
        } else {
          this.registroForm.get('localidad_origen').setValidators(null);
          this.registroForm.get('provincia_origen').setValidators(null);
          this.registroForm.get('distrito_origen').setValidators(null);
          this.registroForm.get('localidad_destino').setValidators(null);
          this.registroForm.get('provincia_destino').setValidators(null);
          this.registroForm.get('distrito_destino').setValidators(null);
          this.registroForm.get('tipo_entierro').setValidators(null);

          this.registroForm.get('compuesto_aplicado_cre').setValidators(null);

          this.registroForm.get('funeraria').setValidators(null);
          this.registroForm.get('ubicacion_funeraria').setValidators(null);
          this.registroForm.get('distrito_funeraria').setValidators(null);
          this.registroForm.get('provincia_funeraria').setValidators(null);
          this.registroForm.get('tipo_conservacion_inh').setValidators(null);
          this.registroForm.get('compuesto_aplicado_inh').setValidators(null);

          this.registroForm.get('tipo_conservacion').setValidators(null);
          this.registroForm.get('tipo_necropsia').setValidators(null);
          this.registroForm.get('certificado_necropsia').setValidators(null);
          this.registroForm.get('entidad').setValidators(null);
          this.registroForm.get('nombre_patologo').setValidators(null);
        }

        this.registroForm.get('localidad_origen').updateValueAndValidity();
        this.registroForm.get('provincia_origen').updateValueAndValidity();
        this.registroForm.get('distrito_origen').updateValueAndValidity();
        this.registroForm.get('localidad_destino').updateValueAndValidity();
        this.registroForm.get('provincia_destino').updateValueAndValidity();
        this.registroForm.get('distrito_destino').updateValueAndValidity();
        this.registroForm.get('tipo_entierro').updateValueAndValidity();

        this.registroForm.get('compuesto_aplicado_cre').updateValueAndValidity();

        this.registroForm.get('funeraria').updateValueAndValidity();
        this.registroForm.get('ubicacion_funeraria').updateValueAndValidity();
        this.registroForm.get('distrito_funeraria').updateValueAndValidity();
        this.registroForm.get('provincia_funeraria').updateValueAndValidity();
        this.registroForm.get('tipo_conservacion_inh').updateValueAndValidity();
        this.registroForm.get('compuesto_aplicado_inh').updateValueAndValidity();

        this.registroForm.get('tipo_conservacion').updateValueAndValidity();
        this.registroForm.get('tipo_necropsia').updateValueAndValidity();
        this.registroForm.get('certificado_necropsia').updateValueAndValidity();
        this.registroForm.get('entidad').updateValueAndValidity();
        this.registroForm.get('nombre_patologo').updateValueAndValidity();
      });
  }

  showtipo_conservacion;
  showtipoconservacion(tipo_conservacion) {
    if (tipo_conservacion == 'FORMOLIZACIÓN') {
      this.showtipo_conservacion = true;
    } else {
      this.showtipo_conservacion = false;
    }
  }

  showtipo_necropsia;
  showtipo_necropsia_mp;
  showtiponecropsia(tipo_necropsia) {
    this.showtipo_necropsia = false;
    this.showtipo_necropsia_mp = false;
    if (tipo_necropsia == 'Autorización cremación') {
      this.showtipo_necropsia_mp = true;
      this.showtipo_necropsia = true;
    } else {
      this.showtipo_necropsia_mp = false;
      this.showtipo_necropsia = true;
    }
  }
  showEstablecimiento() {
    this.registroForm.get('lugar_ocurrencia').valueChanges
      .subscribe(lugar_ocurrencia => {
        this.registroForm.get('establecimiento').reset();
        this.registroForm.get('direccion_fallecimiento').reset();
        if (lugar_ocurrencia === '1') {
          this.showestablecimiento = true;
          this.registroForm.get('establecimiento').setValidators([Validators.required]);
          this.registroForm.get('direccion_fallecimiento').setValidators(null);
        } else {
          this.showestablecimiento = false;
          this.registroForm.get('establecimiento').setValidators(null);
          this.registroForm.get('direccion_fallecimiento').setValidators([Validators.required]);
        }
        this.registroForm.get('establecimiento').updateValueAndValidity();
        this.registroForm.get('direccion_fallecimiento').updateValueAndValidity();
      });
  }

  listarDistritoPorProvincia(provincia) {
    this.preloadingstate = true;
    this.registroForm.get('distrito').reset();
    this.establecimiento_list = [];
    this.autorizacionService.listarDistritosPorProvincia(provincia)
      .subscribe((data) => {
        this.distrito_list = data.distrito;
        this.f.distrito.enable();
        this.preloadingstate = false;
      });
  }

  listarDistritoPorProvinciaOrigen(provincia) {
    this.preloadingstate = true;
    this.autorizacionService.listarDistritosPorProvincia(provincia)
      .subscribe((data) => {
        this.distritos_list_origen = data.distrito;
        this.preloadingstate = false;
      });
  }

  listarDistritoPorProvinciaDestino(provincia) {
    this.preloadingstate = true;
    this.autorizacionService.listarDistritosPorProvincia(provincia)
      .subscribe((data) => {
        this.distritos_list_destino = data.distrito;
        this.preloadingstate = false;
      });
  }

  listarDistritoPorProvinciaFuneraria(provincia) {
    this.preloadingstate = true;
    this.autorizacionService.listarDistritosPorProvincia(provincia)
      .subscribe((data) => {
        this.distritos_list_funeraria = data.distrito;
        this.preloadingstate = false;
      });
  }

  listarEstablecimiento(provinciaid, distritoid) {
    this.preloadingstate = true;
    this.autorizacionService.listarEstablecimiento(provinciaid, distritoid)
      .subscribe((data) => {
        this.establecimiento_list = data.establecimiento;
        this.f.establecimiento.enable();
        this.preloadingstate = false;
      });
  }


  showdetalleautorizacion;
  requisito_list_natural;
  requisito_list_subita;
  requisito_list_48h;
  listarRequisitosPorTipoAutorizacion(id) {
    this.showdetalleautorizacion = id;
    this.requisito_list_natural = [];
    this.requisito_list_subita = [];
    this.requisito_list_48h = [];
    if (id != 4) {
      this.listarrequisitos(id);
    }
  }

  listarrequisitos(id) {
    this.preloadingstate = true;
    this.autorizacionService.listarRequisitosPorTipoAutorizacion(id)
      .subscribe((data) => {
        this.requisito_list = data.requisito;
        for (let i = 0; i < data.requisito.length; i++) {
          if (data.requisito[i].tipo_muerte == 1) {
            this.requisito_list_natural.push(data.requisito[i]);
          } else if (data.requisito[i].tipo_muerte == 2) {
            this.requisito_list_subita.push(data.requisito[i]);
          } else {
            this.requisito_list_48h.push(data.requisito[i]);
          }
        }
        this.preloadingstate = false;
      });
  }
  filtrarrequisitos(tipo_entierro) {
    this.preloadingstate = true;
    this.requisito_list_natural = [];
    this.requisito_list_subita = [];
    this.requisito_list_48h = [];
    if (tipo_entierro == 'INHUMACIÓN') {
      tipo_entierro = 1;
    } else {
      tipo_entierro = 2;
    }
    this.autorizacionService.listarRequisitosPorTipoAutorizacion(4)
      .subscribe((data) => {
        this.requisito_list = data.requisito;
        for (let i = 0; i < data.requisito.length; i++) {
          if (data.requisito[i].tipo_muerte == 1 && data.requisito[i].tipo_entierro == tipo_entierro) {
            this.requisito_list_natural.push(data.requisito[i]);
          } else if (data.requisito[i].tipo_muerte == 2 && data.requisito[i].tipo_entierro == tipo_entierro) {
            this.requisito_list_subita.push(data.requisito[i]);
          } else if (data.requisito[i].tipo_muerte == 3 && data.requisito[i].tipo_entierro == tipo_entierro) {
            this.requisito_list_48h.push(data.requisito[i]);
          }
        }
        this.preloadingstate = false;
      });
  }

  buscarSolicitante() {
    this.buscarSolicitanteSubmitted = true;
    if (!this.f.dni_solicitante.errors) {
      this.preloadingstate = true;
      this.autorizacionService.buscarSolicitantePorDNI(this.f.dni_solicitante.value)
        .subscribe((data) => {
          if (data.status == true && data.code == 1) {
            this.solicitante = data.solicitante;
            if (this.solicitante == null) {
              this.autorizacionService.consultaDNIRegional(this.f.dni_solicitante.value)
                .subscribe((data) => {
                  if (data.resultado != null) {
                    this.solicitante = data.resultado;
                    if (this.solicitante != null) {
                      this.registroForm.patchValue({
                        nombres_solicitante: this.solicitante.NOMBRES,
                        apellidos_solicitante: this.solicitante.APPAT + ' ' + this.solicitante.APMAT
                      });
                    } else {
                      swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Error al buscar persona, intente nuevamente',
                      })
                    }
                    this.preloadingstate = false;
                  }
                });
            } else {
              this.registroForm.patchValue({
                nombres_solicitante: this.solicitante.nombres,
                apellidos_solicitante: this.solicitante.apellidos
              });
            }
            this.f.telefono_solicitante.enable();
            this.f.correo_solicitante.enable();
            this.f.direccion_solicitante.enable();
            this.f.parentesco.enable();
            this.preloadingstate = false;
          }
        });
    }
  }

  buscarFallecido() {
    this.buscarFallecidoSubmitted = true;
    if (!this.f.dni_fallecido.errors) {
      this.preloadingstate = true;
      this.autorizacionService.buscarFallecidoPorDNI(this.f.dni_fallecido.value)
        .subscribe((data) => {
          if (data.status == true && data.code == 1) {
            this.fallecido = data.fallecido;
            if (this.fallecido == null) {
              this.autorizacionService.consultaDNIRegional(this.f.dni_fallecido.value)
                .subscribe((data) => {
                  if (data.resultado != null) {
                    this.fallecido = data.resultado;
                    if (this.fallecido != null) {
                      this.registroForm.patchValue({
                        nombres_fallecido: this.fallecido.NOMBRES,
                        apellidos_fallecido: this.fallecido.APPAT + ' ' + this.fallecido.APMAT
                      });
                      this.f.edad_fallecido.enable();
                      this.f.fecha_fallecimiento.enable();
                      this.f.lugar_ocurrencia.enable();
                      this.f.provincia.enable();
                      this.f.direccion_fallecimiento.enable();
                      this.f.ciex.enable();
                      this.preloadingstate = false;
                    } else {
                      swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Error al buscar persona, intente nuevamente',
                      })
                    }
                  }
                });
            } else {
              swal.fire({
                position: 'center',
                icon: 'error',
                title: 'El fallecido se encuentra registrado',
              })
              this.preloadingstate = false;
            }
          }
        });
    }
  }

  buscarMedico() {
    this.buscarMedicoSubmitted = true;
    if (!this.f.dni_medico.errors) {
      this.preloadingstate = true;
      this.autorizacionService.buscarSolicitantePorDNI(this.f.dni_medico.value)
        .subscribe((data) => {
          if (data.status == true && data.code == 1) {
            this.medico = data.medico;
            if (this.medico == null) {
              this.autorizacionService.consultaDNIRegional(this.f.dni_medico.value)
                .subscribe((data) => {
                  if (data.resultado != null) {
                    this.medico = data.resultado;
                    if (this.medico != null) {
                      this.registroForm.patchValue({
                        nombres_medico: this.medico.NOMBRES,
                        apellidos_medico: this.medico.APPAT + ' ' + this.medico.APMAT
                      });
                    } else {
                      swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Error al buscar persona, intente nuevamente',
                      })
                    }
                    this.preloadingstate = false;
                  }
                });
            } else {
              this.f.nombres_medico.setValue(this.medico.nombres);
              this.f.apellidos_medico.setValue(this.medico.apellidos);
            }
            this.preloadingstate = false;
          }
        });
    }
  }
  onSubmitRegistrar() {
    let dataporenviar = this.registroForm.getRawValue();
    this.registrarSubmitted = true;
    if (this.registroForm.invalid) {
      return;
    }
    this.preloadingstate = true;
    let formData = new FormData();
    formData.append('autorizacion', JSON.stringify(dataporenviar));
    if (this.archivoRequisito.length == this.requisito_list.length) {
      for (let i = 0; i < this.archivoRequisito.length; i++) {
        formData.append('requisito[' + i + ']', this.archivoRequisito[i].id);
        formData.append('archivo[' + i + ']', this.archivoRequisito[i].archivo);
      }
    } else {
      swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Debe completar los requisitos para el trámite seleccionado.',
      })
      this.preloadingstate = false;
      return;
    }
    this.autorizacionService.registrarAutorizacion(formData)
      .subscribe((data) => {
        swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Solicitud creada con éxito.',
        })
        this.preloadingstate = false;
        //this.ngOnInit();
      });
  }

  keyPressOnlyNumbers(event) {
    if (!((event.keyCode >= 48) && (event.keyCode <= 57))) {
      return false;
    }
  }

  adjuntarRequisitos(event, requisito) {
    if (event.target.files[0].type == 'application/pdf') {
      let requisito_archivo = { id: requisito.id, archivo: <File>event.target.files[0] };
      this.archivoRequisito.push(requisito_archivo);
    } else {
      swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Por favor adjuntar solo archivo PDF',
      });
    }
    console.log(this.archivoRequisito);
  }
}
