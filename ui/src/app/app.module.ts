import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeEsAr from '@angular/common/locales/es-AR';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AutorizacionSanitariaComponent } from './autorizacion-sanitaria/autorizacion-sanitaria.component';
import { ListarAutorizacionSanitariaComponent } from './listar-autorizacion-sanitaria/listar-autorizacion-sanitaria.component';
import { ListarAutorizacionSanitariaFinalComponent } from './listar-autorizacion-sanitaria-final/listar-autorizacion-sanitaria-final.component';
import { AutorizacionSaneamietoAmbientalComponent } from './autorizacion-saneamieto-ambiental/autorizacion-saneamieto-ambiental.component';
import { ListarAutorizacionSaneamientoAmbientalComponent } from './listar-autorizacion-saneamiento-ambiental/listar-autorizacion-saneamiento-ambiental.component';

registerLocaleData(localeEsAr, 'es-Ar');
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AutorizacionSanitariaComponent,
    ListarAutorizacionSanitariaComponent,
    ListarAutorizacionSanitariaFinalComponent,
    AutorizacionSaneamietoAmbientalComponent,
    ListarAutorizacionSaneamientoAmbientalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgSelectModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: AutorizacionSanitariaComponent },
      { path: 'login', component: LoginComponent },
      { path: 'listar-autorizacion', component: ListarAutorizacionSanitariaComponent },
      { path: 'autorizacion-saneamiento-ambiental', component: AutorizacionSaneamietoAmbientalComponent },
      { path: 'listar-autorizacion-saneamiento-ambiental', component: ListarAutorizacionSaneamientoAmbientalComponent },
    ])
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es-Ar' }],
  bootstrap: [AppComponent]
})
export class AppModule { }


