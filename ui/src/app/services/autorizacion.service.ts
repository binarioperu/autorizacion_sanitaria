import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

//const publicEndpoint = 'http://190.116.184.130:/api/';
//const localEndpoint = 'http://172.16.0.5:/api/';

const localEndpoint = 'http://localhost:8001/api/';
//const localEndpoint = 'http://binario.pe/autorizacion_sanitaria/server/api/';

@Injectable({
  providedIn: 'root'
})
export class AutorizacionService {

  constructor(private http: HttpClient) { }

  getLocalIp(): any {
    return this.http.get('http://api.ipify.org/?format=json');
  }

  listarParentesco(): any {
    return this.http.get(localEndpoint + 'parentesco/listar');
  }
  listarTipoDocumento(): any {
    return this.http.get(localEndpoint + 'tipo_documento/listar');
  }

  listarProvincia(): any {
    return this.http.get(localEndpoint + 'provincia/listar');
  }

  listarDistritosPorProvincia(provinciaid): any {
    return this.http.get(localEndpoint + 'distrito/listar/' + provinciaid);
  }

  listarCiex(): any {
    return this.http.get(localEndpoint + 'ciex/listar');
  }

  listarLugarOcurrencia(): any {
    return this.http.get(localEndpoint + 'lugar_ocurrencia/listar');
  }

  listarTipoAutorizacion(): any {
    return this.http.get(localEndpoint + 'tipo_autorizacion/listar');
  }
  listarFuneraria(): any {
    return this.http.get(localEndpoint + 'funeraria/listar');
  }

  consultaDNIRegional(dni): any {
    return this.http.get('http://www.regionlalibertad.gob.pe/PIDE/RENIEC/ConsultaDNI/' + dni);
  }

  buscarSolicitantePorDNI(dni): any {
    return this.http.get(localEndpoint + 'solicitante/dni/' + dni);
  }

  buscarFallecidoPorDNI(dni): any {
    return this.http.get(localEndpoint + 'fallecido/dni/' + dni);
  }

  listarRequisitosPorTipoAutorizacion(id): any {
    return this.http.get(localEndpoint + 'requisito/autorizacion/' + id + '/listar');
  }

  registrarAutorizacion(formData: Object): any {
    return this.http.post(localEndpoint + 'autorizacion/registrar', formData);
  }
  listarSolicitud(): any {
    return this.http.get(localEndpoint + 'autorizacion/listar/1');
  }
  getFile(item) {
    return this.http.post(localEndpoint + 'downloadfile', item, {
      responseType: 'blob'
    });
  }
  listarAutorizacionesPendientes(): any {
    return this.http.get(localEndpoint + 'autorizacion_pendiente/listar');
  }
  listarAutorizacionesAprobadas(): any {
    return this.http.get(localEndpoint + 'autorizacion_aprobada/listar');
  }
  listarEstablecimiento(provinciaid, distritoid): any {
    return this.http.get(localEndpoint + 'establecimiento/listar/' + provinciaid + '/' + distritoid);
  }
  updateAutorizacion(estado, autorizacion_id): any {
    return this.http.get(localEndpoint + 'autorizacion/actualizar/' + estado + '/' + autorizacion_id);
  }
  listarAutorizacionFinal(): any {
    return this.http.get(localEndpoint + 'autorizacionfinal/listar');
  }
  registrarAutorizacionSaneamientoAmbiental(formData: Object): any {
    return this.http.post(localEndpoint + 'autorizacion/registrar_saneamiento_ambiental', formData);
  }
  login(formData: Object): any {
    return this.http.post(localEndpoint + 'auth/login', formData);
  }
  listarAutorizacionSaneamientoAmbiental(): any {
  return this.http.get(localEndpoint + 'autorizacionsaneamientoambiental/listar');
  }
}
