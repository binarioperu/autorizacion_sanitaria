import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionSaneamietoAmbientalComponent } from './autorizacion-saneamieto-ambiental.component';

describe('AutorizacionSaneamietoAmbientalComponent', () => {
  let component: AutorizacionSaneamietoAmbientalComponent;
  let fixture: ComponentFixture<AutorizacionSaneamietoAmbientalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionSaneamietoAmbientalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionSaneamietoAmbientalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
