import { Component, OnInit } from '@angular/core';
import { AutorizacionService } from '../services/autorizacion.service';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-autorizacion-saneamieto-ambiental',
  templateUrl: './autorizacion-saneamieto-ambiental.component.html',
  styleUrls: ['./autorizacion-saneamieto-ambiental.component.css']
})
export class AutorizacionSaneamietoAmbientalComponent implements OnInit {
  title = 'SOLICITUD DE COSTANCIA DE INSPECCION TECNICA DE ACTIVIDADES DE EMPRESAS DE SANEAMIENTO AMBIENTAL';
  registrarSubmitted;
  buscarSolicitanteSubmitted;
  buscarFallecidoSubmitted;
  buscarMedicoSubmitted;
  registroForm: FormGroup;
  detalleautorizacionForm: FormGroup;
  preloadingstate: boolean = false;
  provincia_list;
  distrito_list;
  distritos_list_empresa;
  tipo_documento_list;
  archivoRequisito;
  propietario = null;
  mostrardiv;
  servicios_saneamiento = [{ id: 1, descripcion: 'Desinsectacion', checked: '' }, { id: 2, descripcion: 'Desratizacion', checked: '' }, { id: 3, descripcion: 'Desinfección', checked: '' }, { id: 4, descripcion: 'Limpieza de Ambientes', checked: '' }, { id: 5, descripcion: 'Limpieza y Desinfeccion de Reservorios de Agua', checked: '' }, { id: 6, descripcion: 'Limpieza de Tanques Septicos', checked: '' }]

  constructor(private formBuilder: FormBuilder, private titleService: Title, private autorizacionService: AutorizacionService) { }

  ngOnInit(): void {
    this.registrarSubmitted = false;
    this.buscarSolicitanteSubmitted = false;
    this.buscarMedicoSubmitted = false;
    this.archivoRequisito = [];
    this.distritos_list_empresa = [];
    this.tipo_documento_list = [];
    this.distrito_list = [];
    this.provincia_list = [];
    this.mostrardiv = 'datosrepresentante';
    this.titleService.setTitle(this.title);
    this.beginFormBuilder();
    this.listarProvincia();
    this.listarTipoDocumento();
  }
  beginFormBuilder() {
    this.registroForm = this.formBuilder.group({
      dni_propietario: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      nombres_propietario: [{ value: '', disabled: true }, Validators.required],
      apellidos_propietario: [{ value: '', disabled: true }, Validators.required],
      telefono_propietario: [{ value: '', disabled: true }, Validators.required],
      celular_propietario: [{ value: '', disabled: true }, Validators.required],
      correo_propietario: [{ value: '', disabled: true }, Validators.required],
      domicilio_propietario: [{ value: '', disabled: true }, Validators.required],
      distrito_propietario: [{ value: '', disabled: true }, Validators.required],
      provincia_propietario: [{ value: '', disabled: true }, Validators.required],

      nombre_comercial: [{ value: '', disabled: true }, Validators.required],
      numero_ruc: [{ value: '', disabled: true }, Validators.required],
      telefono_empresa: [{ value: '', disabled: true }, Validators.required],
      celular_empresa: [{ value: '', disabled: true }, Validators.required],
      direccion_empresa: [{ value: '', disabled: true }, Validators.required],
      provincia_empresa: [{ value: '', disabled: true }, Validators.required],
      distrito_empresa: [{ value: '', disabled: true }, Validators.required],
      correo_electronico_empresa: [{ value: '', disabled: true }, Validators.required],
      servicios_saneamiento: [''],
    });
  }
  get f() { return this.registroForm.controls; }

  listarTipoDocumento() {
    this.preloadingstate = true;
    this.autorizacionService.listarTipoDocumento()
      .subscribe((data) => {
        this.tipo_documento_list = data.tipo_documento;
        this.preloadingstate = false;
      });
  }

  listarProvincia() {
    this.preloadingstate = true;
    this.autorizacionService.listarProvincia()
      .subscribe((data) => {
        this.provincia_list = data.provincia;
        this.preloadingstate = false;
      });
  }


  listarDistritoPorProvincia(provincia) {
    this.preloadingstate = true;
    this.registroForm.get('distrito_propietario').reset();
    this.autorizacionService.listarDistritosPorProvincia(provincia)
      .subscribe((data) => {
        this.distrito_list = data.distrito;
        this.f.distrito_propietario.enable();
        this.preloadingstate = false;
      });
  }

  listarDistritoPorProvinciaEmpresa(provincia) {
    this.preloadingstate = true;
    this.autorizacionService.listarDistritosPorProvincia(provincia)
      .subscribe((data) => {
        this.distritos_list_empresa = data.distrito;
        this.preloadingstate = false;
      });
  }
  buscarSolicitante() {
    this.buscarSolicitanteSubmitted = true;
    if (!this.f.dni_propietario.errors) {
      this.preloadingstate = true;
      this.autorizacionService.buscarSolicitantePorDNI(this.f.dni_propietario.value)
        .subscribe((data) => {
          if (data.status == true && data.code == 1) {
            this.propietario = data.propietario;
            if (this.propietario == null) {
              this.autorizacionService.consultaDNIRegional(this.f.dni_propietario.value)
                .subscribe((data) => {
                  if (data.resultado != null) {
                    this.propietario = data.resultado;
                    if (this.propietario != null) {
                      this.registroForm.patchValue({
                        nombres_propietario: this.propietario.NOMBRES,
                        apellidos_propietario: this.propietario.APPAT + ' ' + this.propietario.APMAT
                      });
                    } else {
                      swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Error al buscar persona, intente nuevamente',
                      })
                    }
                    this.preloadingstate = false;
                  }
                });
            } else {
              this.registroForm.patchValue({
                nombres_propietario: this.propietario.nombres,
                apellidos_propietario: this.propietario.apellidos,
              });
            }
            this.f.telefono_propietario.enable();
            this.f.celular_propietario.enable();
            this.f.correo_propietario.enable();
            this.f.domicilio_propietario.enable();
            this.f.distrito_propietario.enable();
            this.f.provincia_propietario.enable();
            this.f.nombre_comercial.enable();
            this.f.numero_ruc.enable();
            this.f.telefono_empresa.enable();
            this.f.celular_empresa.enable();
            this.f.direccion_empresa.enable();
            this.f.provincia_empresa.enable();
            this.f.distrito_empresa.enable();
            this.f.correo_electronico_empresa.enable();
            this.preloadingstate = false;
          }
        });
    }
  }
  onSubmitRegistrar() {
    this.registroForm.patchValue({
      servicios_saneamiento: this.arrayservicios_saneamiento,
    });
    let dataporenviar = this.registroForm.getRawValue();
    this.registrarSubmitted = true;
    if (this.registroForm.invalid) {
      return;
    }
    this.preloadingstate = true;
    let formData = new FormData();
    formData.append('autorizacion', JSON.stringify(dataporenviar));
    if (this.archivoRequisito.length == 8) {
      for (let i = 0; i < this.archivoRequisito.length; i++) {
        formData.append('requisito[' + i + ']', this.archivoRequisito[i].id);
        formData.append('archivo[' + i + ']', this.archivoRequisito[i].archivo);
      }
    } else {
      swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Debe completar los requisitos para el trámite seleccionado.',
      })
      this.preloadingstate = false;
      return;
    }
    this.autorizacionService.registrarAutorizacionSaneamientoAmbiental(formData)
      .subscribe((data) => {
        swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Solicitud creada con éxito.',
        })
        this.preloadingstate = false;
        this.ngOnInit();
      });
  }

  keyPressOnlyNumbers(event) {
    if (!((event.keyCode >= 48) && (event.keyCode <= 57))) {
      return false;
    }
  }

  arrayservicios_saneamiento = [];
  onChange(id, isChecked: boolean) {
    if (isChecked) {
      this.arrayservicios_saneamiento.push(id);
      for (let i = 0; i < this.servicios_saneamiento.length; i++) {
        if (this.servicios_saneamiento[i].id == id) {
          this.servicios_saneamiento[i].checked = 'checked';
        }
      }
    } else {
      let index = this.arrayservicios_saneamiento.indexOf(id);
      this.arrayservicios_saneamiento.splice(index, 1);
      for (let i = 0; i < this.servicios_saneamiento.length; i++) {
        if (this.servicios_saneamiento[i].id == id) {
          this.servicios_saneamiento[i].checked = '';
        }
      }
    }
    console.log(this.arrayservicios_saneamiento);
  }


  adjuntarRequisitos(event, requisito) {
    if (event.target.files[0].type == 'application/pdf') {
      let requisito_archivo = { id: requisito, archivo: <File>event.target.files[0] };
      this.archivoRequisito.push(requisito_archivo);
    } else {
      swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Por favor adjuntar solo archivo PDF',
      });
    }
    console.log(this.archivoRequisito);
  }
}
