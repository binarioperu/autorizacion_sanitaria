import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarAutorizacionSanitariaComponent } from './listar-autorizacion-sanitaria.component';

describe('ListarAutorizacionSanitariaComponent', () => {
  let component: ListarAutorizacionSanitariaComponent;
  let fixture: ComponentFixture<ListarAutorizacionSanitariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarAutorizacionSanitariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarAutorizacionSanitariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
