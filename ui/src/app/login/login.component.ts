import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AutorizacionService } from './../services/autorizacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submitted = false;
  formulario: FormGroup;
  usuarioincorrecto: boolean;
  mensajelogg;
  sesionuser;
  preloadingstate: boolean;

  constructor(private router: Router, private formBuilder: FormBuilder,private servicio:AutorizacionService
    ) { }

  ngOnInit(): void {
    this.formulario = this.formBuilder.group({
      user: ['', [Validators.required]],
      passw: ['', [Validators.required]]
    });
  }
  get f() { return this.formulario.controls; }

  login() {
    this.submitted = true;
    if (this.formulario.invalid) {
      return;
    }
    this.preloadingstate = true;
    let formData = new FormData();
    formData.append('usuario', this.formulario.value.user);
    formData.append('password', this.formulario.value.passw);
    try {
      this.servicio.login(formData)
        .subscribe((data) => {
          if (data.status == true) {
            if (data.usuario != null) {
              localStorage.setItem('user', JSON.stringify(data));
              this.router.navigate(["/listar-autorizacion"]);
            } else {
              this.usuarioincorrecto = true;
              this.mensajelogg = 'No tiene los permisos necesarios';
            }
          } else {
            this.usuarioincorrecto = true;
            this.mensajelogg = 'Datos incorrectos';
          }
          this.preloadingstate = false;
        },
          err => console.log(err),
        );
    }
    catch (e) {
      console.log(e);
    }
  }


}
