import { Component, OnInit } from '@angular/core';
import { AutorizacionService } from '../services/autorizacion.service';

@Component({
  selector: 'app-listar-autorizacion-sanitaria-final',
  templateUrl: './listar-autorizacion-sanitaria-final.component.html',
  styleUrls: ['./listar-autorizacion-sanitaria-final.component.css']
})
export class ListarAutorizacionSanitariaFinalComponent implements OnInit {

  constructor(private autorizacionService: AutorizacionService) { }

  preloadingstate;
  autorizaciones_list;

  ngOnInit(): void {
    this.preloadingstate = true;
    this.autorizacionService.listarAutorizacionFinal()
      .subscribe((data) => {
        this.autorizaciones_list = data.autorizaciones;
        this.preloadingstate = false;
      });
  }
}
