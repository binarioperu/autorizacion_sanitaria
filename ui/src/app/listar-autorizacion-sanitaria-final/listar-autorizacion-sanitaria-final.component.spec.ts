import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarAutorizacionSanitariaFinalComponent } from './listar-autorizacion-sanitaria-final.component';

describe('ListarAutorizacionSanitariaFinalComponent', () => {
  let component: ListarAutorizacionSanitariaFinalComponent;
  let fixture: ComponentFixture<ListarAutorizacionSanitariaFinalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarAutorizacionSanitariaFinalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarAutorizacionSanitariaFinalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
